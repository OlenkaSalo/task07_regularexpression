package TextPatern;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentences {
    public List<String> sentences(StringBuilder text) {

        String pattern = "[A-Z]+[^.!?\\s][^.!?]*(?:[.!?](?!['\"]?\\s|$)[^.!?]*)*[.!?]?['\"]?(?=\\s|$)";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);
        List<String> list = new LinkedList<>();
        while (m.find()) {
           list.add((text.substring(m.start(), m.end())));

        }
        return list;
        }

    }

