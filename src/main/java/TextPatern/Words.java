package TextPatern;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Words {
    public List<String> findWords(StringBuilder text)
    {
        String pattern = "[a-zA-Z]+?\\W|$";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);
        List<String> listWord = new ArrayList<>();
        while(m.find())
        {
            listWord.add(m.group().replaceAll("\\W",""));
        }
        return listWord;

    }
}
