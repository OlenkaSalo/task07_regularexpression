package Main;
import Menu.MainMenu;
import tasks.*;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringFile {
    public static void main(String[] args) throws IOException {


        File file = new File("D:\\Helen!\\Java\\Thinking_inJava.txt");
        BufferedReader bufferedReader =null;
        bufferedReader =new BufferedReader(new FileReader(file));
        String inputLine = null;
        StringBuilder sb = new StringBuilder();

        String pattern = "\\s";
        Pattern p = Pattern.compile(pattern);

        DeleteWord_12 dW_12 = new DeleteWord_12();

        try{
            while((inputLine =bufferedReader.readLine())!= null) {
                if(inputLine.trim().length()>0)
                {
                  inputLine=inputLine.trim().replaceAll("\\s+", " ")+" ";
                }

                Matcher m = p.matcher(inputLine);
               if (m.find()) {
                   inputLine.trim().replaceAll("\\s", " ");
                   sb.append(inputLine);
               }
//                dW_12.deleteWord(inputLine);
            }


MainMenu mainMenu = new MainMenu(sb);
            mainMenu.show();


        } finally {
            bufferedReader.close();
        }
    }
}
