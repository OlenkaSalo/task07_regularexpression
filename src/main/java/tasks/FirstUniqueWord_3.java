package tasks;

import TextPatern.Sentences;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class FirstUniqueWord_3 {

    public void uniqueWord(StringBuilder text) {
        String pattern = "[A-Z]+[^.!?\\s][^.!?]*(?:[.!?](?!['\"]?\\s|$)[^.!?]*)*[.!?]?['\"]?(?=\\s|$)";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);
        List<String> set = new ArrayList<>();
        List<String> unique = unWord(text.toString());
        System.out.println(unique);
        int i=0;
        while (m.find()&&i<1) {
            String [] word =text.substring(m.start(),m.end()).split(" ");
           for(String words:word){
            if(unique.contains(words))
                set.add(words);
            }
            System.out.println(set.get(0));
            i++;
        }
        }

   public List<String> unWord(String s){
        String str = s.replaceAll("\\W"," ");
        List<String> list = Arrays.asList(str.split(" [!-~]* "));
        list.stream()
                .filter(i->i.length()>2)
                .distinct()
                .collect(Collectors.toList());
        return list;
    }
}
