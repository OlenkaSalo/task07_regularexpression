package tasks;

import TextPatern.Sentences;

import java.util.*;



public class increaseSentences_2 {

    public void increaseSentences(StringBuilder text) {
        Sentences sen = new Sentences();
        List<String> list = sen.sentences(text);
        Map <String,Long>orderSent= new HashMap< String,Long>();
        Map<String,Long>result = new LinkedHashMap<>();

            for(String s: list) {
                orderSent.put(s, countWordOfSentences(s));
            }

        orderSent.entrySet()
                .stream()
                .sorted(Map.Entry.<String,Long>comparingByValue())
                .forEachOrdered(x -> result.put(x.getKey(), x.getValue()));
        printMap(result);

    }

    public Long countWordOfSentences(String input)
    {
        List<String> list = Arrays.asList(input.split(" "));
                   Long count = list.stream()
                   .filter(i -> i.length() > 2)
                   .count();
            return count;
    }
    public static <Long, String> void printMap(Map<Long, String> map) {
       for (Map.Entry<Long, String> entry : map.entrySet()) {
          System.out.println("Key : " + entry.getKey()
               + " Value : " + entry.getValue());
     }
    }
}
