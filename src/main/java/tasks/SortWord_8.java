package tasks;

import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SortWord_8 {

    public void sortWord_8(StringBuilder text) {
        String pattern = "[a-zA-Z]+?\\W|$";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);
        List<String> list = new ArrayList<>();
        Map<String,String> map = new HashMap<>();

                while (m.find()) {
            String str = m.group().replaceAll("\\W", " ");
            list.addAll(firstVowelLetter(str));
        }
              for(String l:list)
              {
                  map.put(firstConsonLetter(l),l);

              }
       map.entrySet()
               .stream()
               .sorted(Map.Entry.comparingByKey())
               .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1,e2)->e1, LinkedHashMap::new))
               .forEach((k,v)->System.out.println("Kye: "+k + " Word: " + v));
    }

    public static List<String> firstVowelLetter(String word) {
        Pattern vowelPattern = Pattern.compile("[aeiou]", Pattern.CASE_INSENSITIVE);
        Matcher vowelMatcher = vowelPattern.matcher(word);
        List<String> list = Arrays.asList(word.split(" "));
        List<String> list1 = new ArrayList<>();
        while (vowelMatcher.find()) {
            Optional<String> opt = list.stream()
                    .filter(l -> l.length() > 2)
                    .filter(i -> i.startsWith(word.substring(vowelMatcher.start(), vowelMatcher.end())))
                    .findAny();

            if (opt.isPresent()) {
                list1.add(String.valueOf(opt.get()));
            }
        }
        return list1;
    }

    public static String firstConsonLetter(String word) {
        List<String> words = Arrays.asList(word.split(" "));
        List<String> list1 = new ArrayList<>();
        Pattern conPattern = Pattern.compile("[bcdfghjklmnpqrstvwxz]");
        Matcher conMatcher = conPattern.matcher(words.toString());
        String str = null;
        int count =0;
        while (conMatcher.find()&& count<1) {

            str = conMatcher.group();
            list1.add(str);
            count++;
        }

        return str;
    }
}
