package tasks;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SortWord_9 {

    public void sortWord_9(StringBuilder text) {
        String pattern = "[a-zA-Z]+?\\W|$";
        Pattern p = Pattern.compile(pattern);
        Matcher mat = p.matcher(text);
        Map<String, Long> map = new HashMap<String, Long>();
        Map<String, Long> result = new LinkedHashMap<>();
        increaseSentences_2 iS = new increaseSentences_2();
        while (mat.find()) {
            map.put(mat.group().replaceAll("\\W"," "), countLetter(mat.group().replaceAll("\\W"," ")));
        }
                     map.entrySet()
                        .stream()
                        .sorted(Map.Entry.comparingByValue())
                        .forEachOrdered(x -> result.put(x.getKey(), x.getValue()));

                         result.forEach((k,v)->{
                            if(k.length() == k.length())
                            {
                                  result.entrySet()
                                          .stream()
                                          .sorted(Map.Entry.comparingByKey())
                                          .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,(e1,e2)->e1, LinkedHashMap::new));
            }
        });
        iS.printMap(result);
    }

    public static Long countLetter(String word)
    {
        List<String> list = Arrays.asList(word.split(" "));
        long count =list.stream()
                        .filter(l->l.length()>2)
                        .filter(i->i.contains("e"))
                        .count();
        return count;
    }
}
