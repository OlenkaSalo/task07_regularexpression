package tasks;

import TextPatern.Sentences;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.summingInt;

public class ContainerWords_10 {

    public void containWords(StringBuilder text) {
        Sentences sen = new Sentences();
        List<String> listSentences = sen.sentences(text);
        List<String> listWords = new ArrayList<>();
        List<String> container = new ArrayList<>();
        listWords.add("level");
        listWords.add("second");
        listWords.add("generics");
        listWords.add("code");
        for(String s: listSentences)
        {
            for (String w: listWords)
            {
                if(s.contains(w))
                {
                    container.add(w);
                }
            }
        }
        Map<String, Integer> map = container.stream()
                .flatMap(i-> Arrays.stream(i.split(" ")))
                .collect(Collectors.groupingBy(Function.identity(), summingInt(e -> 1)));
        map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue, (e1,e2)->e1, LinkedHashMap::new))
                .forEach((k,v)->System.out.println("Key: "+k + " Word: " + v));

    }

}
