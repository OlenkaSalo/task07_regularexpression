package tasks;

import TextPatern.Sentences;
import java.util.*;
import java.util.stream.IntStream;

public class Palindrom_14 {

    public void findPalindrom(StringBuilder text) {
        Sentences sen = new Sentences();
        List<String> list = sen.sentences(text);
        String word = "";
        int maxLenth = 0;
        int start=0;
        String  longestPalidrom =null;

        for (String s : list) {

            word = s.toLowerCase().replaceAll("[\\W]", "");
            for (int i = 0; i < word.length(); i++)
                for (int j = i; j < word.length(); j++)
                    if(isPalindromeUsingIntStream(word.substring(i, j + 1))) {
                        if(word.substring(i, j + 1).length()>maxLenth)
                        {
                            start=i;
                            maxLenth=word.substring(i, j + 1).length();

                            longestPalidrom = word.substring(start,start+maxLenth);

                        }
                    }
        }

        System.out.print(longestPalidrom);

    }


    public static boolean isPalindromeUsingIntStream(String text) {
        String temp  = text;
        return IntStream.range(0, temp.length() / 2)
                .noneMatch(i -> temp.charAt(i) != temp.charAt(temp.length() - i - 1));
    }
}
