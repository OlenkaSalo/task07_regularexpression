package tasks;


import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ChangeWord_5 {
    public void changeWord(StringBuilder text){
    String pattern = "[A-Z]+[^.!?\\s][^.!?]*(?:[.!?](?!['\"]?\\s|$)[^.!?]*)*[.!?]?['\"]?(?=\\s|$)";
    Pattern p = Pattern.compile(pattern);
    Matcher m = p.matcher(text);
        while (m.find()) {
       firstCharacter(text.substring(m.start(),m.end()));
        }
    }


    public static void firstCharacter(String s)
    {
        Pattern vowelPattern = Pattern.compile("[aeiou]", Pattern.CASE_INSENSITIVE);
        Matcher vowelMatcher = vowelPattern.matcher(s);

        List<String> list = Arrays.asList(s.split(" "));
        while(vowelMatcher.find()) {
            Optional<String> opt = list.stream()
                    .filter(l->l.length()>2)
                    .filter(i -> i.startsWith(s.substring(vowelMatcher.start(),vowelMatcher.end())))
                    .findFirst();

            if (opt.isPresent()) {
                opt.get();
                System.out.println(s.replace(opt.get(),longestWord(s)));
                break;
            }
        }

    }

    public static String longestWord(String s)
    {
       s= Arrays.stream(s.split(" ")).max(Comparator.comparingInt(String::length)).orElse(null);
       return s;
    }
}
