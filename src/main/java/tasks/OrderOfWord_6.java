package tasks;

import TextPatern.Words;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OrderOfWord_6 {

    public void orderOfword(StringBuilder text)
    {
        Words w= new Words();
        List<String> list = w.findWords(text);

                list.stream()
                        .flatMap(i-> Arrays.stream(i.split(" ")))
                        .filter(e->e.length()>2)
                        .sorted()
                        .forEach(System.out::println);
    }
}
