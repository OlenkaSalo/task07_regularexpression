package tasks;
import TextPatern.Sentences;

import java.util.*;
import java.util.regex.*;
import java.util.stream.Collectors;

public class SentencesOfrepatableWord_1 {
    public void findSentences(StringBuilder text) {
        Sentences senList = new Sentences();
        List<String> list = senList.sentences(text);

        for(String s: list)
        {
            Set<String> string = duplicateWords(s);
                 if (!string.isEmpty()) {
                    System.out.println(s);
                    System.out.println(string);

                 }
        }
    }

    public static Set<String> duplicateWords(String input) {
        if(input == null || input.isEmpty()){
            return Collections.emptySet();
        }

         Set<String> duplicates = new HashSet<>();
         String[] words = input.split(" ");
         Set<String> set = new HashSet<>();
              for(String word : words) {
                if (word.length()>1) {
                  if (!set.add(word)) {
                    duplicates.add(word);

                  }
                }
              }
    return duplicates;
    }
}


