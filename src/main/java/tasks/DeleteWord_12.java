package tasks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DeleteWord_12 {
    public void deleteWord(String text)
    {
        StringBuilder sb = new StringBuilder();

        for (String word : findWord(text))
            if (text.contains(word)) {

             sb.append(text.replaceAll(word, " ")) ;
    }

System.out.println(sb);


    }

    public List<String> findWord(String text)
    {
        List<String> word = new ArrayList<>();
        String pattern = "[a-zA-Z]+?\\W|$";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);
        List<String> list1 = new ArrayList<>();
        Pattern conPattern = Pattern.compile("[bcdfghjklmnpqrstvwxz]", Pattern.CASE_INSENSITIVE);
        Matcher conMatcher = conPattern.matcher( text);
        List<String> list = new ArrayList<>();
        increaseSentences_2 iS = new increaseSentences_2();
        while (m.find()) {
            String str = m.group().replaceAll("\\W", " ");
            list.add(str);

        }
        while(conMatcher.find()) {
            Optional<String> opt = list.stream()
                    .flatMap(i -> Arrays.stream(i.split(" ")))
                    .filter(l -> l.length() > 4)
                    .filter(e -> e.startsWith(conMatcher.group()))
                    .findAny();
            if (opt.isPresent()) {
                word.add( String.valueOf(opt.get()));
            }
        }
     return word;
    }
}
