package Menu;

import tasks.*;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainMenu {
    private Map<String, String> menu;
    private Map<String, Print> taskMenu;
    private static Scanner input = new Scanner(System.in);
    private StringBuilder text;


    private void setMenu() {

        menu = new LinkedHashMap<>();
        menu.put("1", new String("1 - Task 1"));
        menu.put("2", new String("2 - Task 2"));
        menu.put("3", new String("3 - Task 3"));
        menu.put("4", new String("4 - Task 4"));
        menu.put("5", new String("5 - Task 5"));
        menu.put("6", new String("6 - Task 6"));
        menu.put("7", new String("7 - Task 7"));
        menu.put("8", new String("8 - Task 8"));
        menu.put("9", new String("9 - Task 9"));
        menu.put("10", new String("10 - Task 10"));
        menu.put("11", new String("11 - Task 11"));
        menu.put("12", new String("12 - Task 12"));
        menu.put("13", new String("13 - Task 13"));
        menu.put("14", new String("14 - Task 14"));
        menu.put("15", new String("15 - Task 15"));
        menu.put("16", new String("16 - Task 16"));

        menu.put("Exit", new String("Exit"));
    }

   public MainMenu(StringBuilder text) {
        this.text = text;
       setMenu();
       taskMenu = new LinkedHashMap<>();
       taskMenu.put("1", this::task_1);
        taskMenu.put("2", this::task_2);
        taskMenu.put("3", this::task_3);
        taskMenu.put("4", this::task_4);
        taskMenu.put("5", this::task_5);
        taskMenu.put("6", this::task_6);
        taskMenu.put("7", this:: task_7);
       taskMenu.put("8", this::task_8);
       taskMenu.put("9", this::task_9);
       taskMenu.put("10", this::task_10);
       taskMenu.put("11", this::task_11);
       taskMenu.put("12", this::task_12);
       taskMenu.put("13", this::task_13);
       taskMenu.put("14", this:: task_14);
       taskMenu.put("15", this::task_15);
       taskMenu.put("16", this:: task_16);
//
//
//    }
   }

    private void task_1()  {
        SentencesOfrepatableWord_1 sorw = new SentencesOfrepatableWord_1();
        sorw.findSentences(this.text);
    }

    private void task_2()  {
        increaseSentences_2 iS= new increaseSentences_2();
        iS.increaseSentences(this.text);
    }
    private void task_3()  {
        FirstUniqueWord_3 firstUniqueWord_3 = new FirstUniqueWord_3();
        firstUniqueWord_3.uniqueWord(this.text);
    }
    private void task_4()
    {
        InterrogativeSentence_4 interSen = new InterrogativeSentence_4();
        interSen.interrogativeSentence(this.text);
    }
     private void task_5(){
         ChangeWord_5 cW = new ChangeWord_5();
         cW.changeWord(this.text);
     }
    private void task_6(){
        OrderOfWord_6 oW = new OrderOfWord_6();
        oW.orderOfword(this.text);
    }
    private void task_7(){
        ProcentVowel_7 pV_7 = new ProcentVowel_7();
        pV_7.procentVowel(this.text);
    }
    private void task_8()
    {
        SortWord_8 sW_8 = new SortWord_8();
        sW_8.sortWord_8(this.text);
    }

    private void task_9()
    {
        SortWord_9 sW_9 = new SortWord_9();
        sW_9.sortWord_9(this.text);
    }

    private void task_10()
    {
        ContainerWords_10 cW = new ContainerWords_10();
        cW.containWords(this.text);
    }

    private void task_11()
    {
        SubstringofSentence_11 sS_11 = new SubstringofSentence_11();
        sS_11.substringOfSentence(this.text);
    }

    private void task_12()
    {
       System.out.println("Task 12 in Class \"String File\"");
    }

    private void task_13()
    {
        ReversedSortWord_13 rSW_13 = new ReversedSortWord_13();
        rSW_13.reversedSortWord_13(this.text);
    }

    private void task_14()
    {
        Palindrom_14 palindrom_14 = new Palindrom_14();
        palindrom_14.findPalindrom(this.text);
    }

    private void task_15()
    {
        ConvertWord_15 convertWord =new ConvertWord_15();
        convertWord.removeFirstLetter(this.text);
    }

    private void task_16()
    {
        SomeSentences_16 sS_16 = new SomeSentences_16();
        sS_16.changeSubstring(this.text);
    }


    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            if (key.length()<=2) {
                System.out.println(menu.get(key));
            }
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Select task, that you'd like check");
            keyMenu = input.nextLine().toUpperCase();
            try {
                taskMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Exit"));
    }
}
